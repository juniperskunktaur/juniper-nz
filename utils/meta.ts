import { MetaInfo } from 'vue-meta';
export type MetaCollection = {
  title: string,
  description: string,
  image?: {
    url?: string;
    width?: number;
    height?: number;
    type?: string;
    alt?: string;
  }
}

export const extrapolateMeta = (input: MetaCollection): MetaInfo => {
  const { title, description, image } = input;
  const imageMeta = [];
  if (image && image.url) {
    imageMeta.push({ hid: 'og:image', name: 'og:image', property: 'og:image', content: image.url });
    if ((image.width && image.width >= 300) && (image.height && image.height >= 157)) {
      imageMeta.push({ hid: 'twitter:card', name: 'twitter:card', content: 'summary_large_image' });
    }
    if (image.width) {
      imageMeta.push({ hid: 'og:image:width', name: 'og:image:width', property: 'og:image:width', content: image.width.toString() });
    }
    if (image.height) {
      imageMeta.push({ hid: 'og:image:height', name: 'og:image:height', property: 'og:image:height', content: image.height.toString() });
    }
    if (image.type) {
      imageMeta.push({ hid: 'og:image:type', name: 'og:image:type', property: 'og:image:type', content: image.type });
    }
    if (image.alt) {
      imageMeta.push({ hid: 'twitter:image:alt', name: 'twitter:image:alt', content: image.alt });
    }
  }
  return {
    title,
    meta: [
      { hid: 'description', name: 'description', content: description },
      { hid: 'og:title', name: 'og:title', content: title },
      { hid: 'og:description', name: 'og:description', content: description },
      ...imageMeta,
    ],
  }
};
