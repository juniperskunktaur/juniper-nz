---
title: New Blog, who this?
createdAt: 2019-12-10
---

I've decided I'll be making this my place to share things first and foremost. FA and Weasyl and DA are great, but there's nothing quite like hosting your own static site.

I might add comments later using Disqus or something.

As always, hit me up on Telegram, Discord or Twitter!

*Four-armed Hugs*

Juniper
