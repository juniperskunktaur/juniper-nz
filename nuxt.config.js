import moment from 'moment';

// const socialTitle = "🦨 Juniper the Skunktaur's Site 🦨";
const socialDescription = 'Official site for your favourite four-armed skunktaur';

const titleTemplateFunction = function (titleChunk) {
  // If undefined or blank then we don't need the hyphen
  return titleChunk ? `🦨 ${titleChunk} - Juniper's Site 🦨` : "🦨 Juniper the Skunktaur's Site 🦨";
};

const publicURL = process.env.PUBLIC_URL || `https://${process.env.VERCEL_URL}` || 'http://localhost:3000';
const baseURL = `${publicURL}${process.env.BASE_URL || ''}`;
const discordInviteURL = 'https://discord.gg/vTPZAyM';

export default {
  telemetry: true,
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  // mode: 'universal',
  mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'static',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    titleTemplate: titleTemplateFunction,
    meta: [
      {
        hid: 'og:title',
        property: 'og:title',
        content: '',
        template: titleTemplateFunction,
      },
    ],
    link: [
      {
        hid: 'stylesheet-material-design-icons',
        rel: 'stylesheet',
        media: 'all',
        href: 'https://cdn.materialdesignicons.com/5.3.45/css/materialdesignicons.min.css',
      },
      {
        hid: 'author-humanstxt',
        rel: 'author',
        href: 'humans.txt',
      },
    ],
  },

  publicRuntimeConfig: {
    publicURL,
    baseURL,
    discordInviteURL,
  },

  pwa: {
    workbox: {
      runtimeCaching: [
        { urlPattern: '/_nuxt/.*' },
        { urlPattern: 'https://cdn.materialdesignicons.com/5.3.45/.*' },
      ],
    },
    meta: {
      name: "Juniper the Skunktaur's Site",
      author: 'Juniper Skunktaur',
      description: socialDescription,
      lang: 'en-NZ',
      ogHost: baseURL,
      theme_color: '#ccffcc',
      twitterSite: '@skunktaur',
      twitterCard: 'summary',
      twitterCreator: '@skunktaur',
    },
    manifest: {
      name: "Juniper the Skunktaur's Site",
      short_name: 'Juniper.nz',
      description: socialDescription,
      lang: 'en-nz',
    },
  },
  /*
   ** Global CSS
   */
  buefy: {
    css: false,
    materialDesignIcons: false,
  },
  css: ['@/assets/app.scss'],
  loading: {
    color: '#fff',
  },
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    'nuxt-composition-api',
    '@nuxt/typescript-build',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    '@aceforth/nuxt-optimized-images',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy',
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt/content
    '@nuxt/content',
    ['nuxt-canonical', { baseUrl: baseURL }],
    '@nuxtjs/feed',
  ],

  optimizedImages: {
    optimizeImages: true,
    // optimizeImagesInDev: true,
    // defaultImageLoader: 'responsive-loader',
    responsive: {
      // sizes: [320, 640, 960, 1200, 1800, 2400],
      // placeholder: true,
      // placeholderSize: 20,
      adapter: require('responsive-loader/sharp'),
    },
  },

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},

  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {},

  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {},

  generate: {
    fallback: true,
  },

  hooks: {
    'content:file:beforeInsert' (document) {
      if (document.extension === '.md') {
        document.url = `https://juniper.nz/journal/${document.slug}`;
        document.excerpt = document.text.substring(0, 120) + `... <a href="${document.url}">read more...</a>`;
        document.theText = document.text;
      }
    },
  },
  feed () {
    const baseUrlJournal = 'https://juniper.nz/journal';
    const baseLinkFeedJournal = '/feed';
    const feedFormats = {
      rss: { type: 'rss2', file: 'rss.xml' },
      atom: { type: 'atom1', file: 'atom.xml' },
      json: { type: 'json1', file: 'feed.json' },
    };
    const { $content } = require('@nuxt/content');

    const createFeedJournal = async function (feed) {
      feed.options = {
        title: "Juni's Journal",
        description: 'Blog posts from a four-armed skunktaur',
        link: baseUrlJournal,
      };
      const journals = await $content('journals').sortBy('createdAt', 'desc').fetch();

      journals.forEach((journal) => {
        const url = journal.url;

        feed.addItem({
          title: journal.title,
          id: url,
          link: url,
          date: moment(journal.createdAt).toDate(),
          description: journal.excerpt,
          content: journal.theText,
        });
      });
    };

    return Object.values(feedFormats).map(({ file, type }) => ({
      path: `${baseLinkFeedJournal}/${file}`,
      type,
      create: createFeedJournal,
    }));
  },
};
